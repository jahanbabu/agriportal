package bd.com.pmo.a2i.agriportal.model;

/**
 * Created by JK on 9/10/17.
 */

public class Hat {
    String hid, hatName, hatAddress, hatType, hatTime;

    public Hat(String hid, String hatName, String hatAddress, String hatType, String hatTime) {
        this.hid = hid;
        this.hatName = hatName;
        this.hatAddress = hatAddress;
        this.hatType = hatType;
        this.hatTime = hatTime;
    }

    public String getHid() {
        return hid;
    }

    public void setHid(String hid) {
        this.hid = hid;
    }

    public String getHatName() {
        return hatName;
    }

    public String getHatAddress() {
        return hatAddress;
    }

    public void setHatAddress(String hatAddress) {
        this.hatAddress = hatAddress;
    }

    public void setHatName(String hatName) {
        this.hatName = hatName;
    }

    public String getHatType() {
        return hatType;
    }

    public void setHatType(String hatType) {
        this.hatType = hatType;
    }

    public String getHatTime() {
        return hatTime;
    }

    public void setHatTime(String hatTime) {
        this.hatTime = hatTime;
    }

    @Override
    public String toString() {
        return "Hat{" +
                "hid='" + hid + '\'' +
                ", hatName='" + hatName + '\'' +
                ", hatAddress='" + hatAddress + '\'' +
                ", hatType='" + hatType + '\'' +
                ", hatTime='" + hatTime + '\'' +
                '}';
    }
}
