package bd.com.pmo.a2i.agriportal.data.db;

import java.util.List;

import bd.com.pmo.a2i.agriportal.data.db.model.Option;
import bd.com.pmo.a2i.agriportal.data.db.model.Question;
import bd.com.pmo.a2i.agriportal.data.db.model.User;
import io.reactivex.Observable;


/**
 * Created by JahangirKabir on 18/10/17.
 */

public interface DbHelper {

    Observable<Long> insertUser(final User user);

    Observable<List<User>> getAllUsers();

    Observable<List<Question>> getAllQuestions();

    Observable<Boolean> isQuestionEmpty();

    Observable<Boolean> isOptionEmpty();

    Observable<Boolean> saveQuestion(Question question);

    Observable<Boolean> saveOption(Option option);

    Observable<Boolean> saveQuestionList(List<Question> questionList);

    Observable<Boolean> saveOptionList(List<Option> optionList);
}
