package bd.com.pmo.a2i.agriportal.ui.main.hat.hatDetails;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import bd.com.pmo.a2i.agriportal.data.DataManager;
import bd.com.pmo.a2i.agriportal.model.HatItem;
import bd.com.pmo.a2i.agriportal.ui.base.BasePresenter;
import bd.com.pmo.a2i.agriportal.utils.rx.SchedulerProvider;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public class HatDetailsPresenter<V extends HatDetailsMvpView> extends BasePresenter<V>
        implements HatDetailsMvpPresenter<V> {

    private static final String TAG = "HatDetailsPresenter";
    private List<HatItem> hatItems;

    @Inject
    public HatDetailsPresenter(DataManager dataManager,
                               SchedulerProvider schedulerProvider,
                               CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    private void prepareData() {
        hatItems = new ArrayList<>();
        for (int i = 0; i < 25; i++){
            HatItem item = new HatItem(i+"", "ফসল"+" - "+i, ""+i);
            hatItems.add(item);
        }
        getMvpView().updateHatItem(hatItems);
    }

    @Override
    public void onViewPrepared() {
        prepareData();

    }
}
