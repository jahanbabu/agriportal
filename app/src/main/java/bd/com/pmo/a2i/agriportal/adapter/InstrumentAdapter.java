package bd.com.pmo.a2i.agriportal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.model.Instrument;
import butterknife.BindView;
import butterknife.ButterKnife;

public class InstrumentAdapter extends RecyclerView.Adapter<InstrumentAdapter.ViewHolder> {
    private ArrayList<Instrument> instruments;
    private Context context;
    private InstrumentClickListener instrumentClickListener;

    static class ViewHolder extends RecyclerView.ViewHolder{
        private Context context;
        // each data item is just a string in this case
        @BindView(R.id.nameTextView) TextView nameTextView;
        @BindView(R.id.typeTextView) TextView typeTextView;
        @BindView(R.id.imageview) ImageView itemImageView;
        @BindView(R.id.deleteImageView) ImageView deleteImageView;

        View view;
        ViewHolder(Context context, View v) {
            super(v);
            this.context = context;
            this.view = v;
            // 2. Bind Butter knife to this view holder
            ButterKnife.bind(this, itemView);
        }
    }

    public InstrumentAdapter(Context context, ArrayList<Instrument> instruments) {
        this.instruments = instruments;
        this.context = context;
    }

    @Override
    public InstrumentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_instrument, parent, false);

        return new ViewHolder(context, v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.nameTextView.setText(instruments.get(position).getInstrumentName());
        holder.typeTextView.setText(instruments.get(position).getInstrumentType());

        holder.view.setOnClickListener(view -> instrumentClickListener.blockItemClicked(view, position, "row"));
        holder.deleteImageView.setOnClickListener(view -> instrumentClickListener.blockItemClicked(view, position, "delete"));
    }

    @Override
    public int getItemCount() {
        return instruments.size();
    }

    public void setClickListener(InstrumentClickListener clickListener) {
        this.instrumentClickListener = clickListener;
    }

    public interface InstrumentClickListener {
        void blockItemClicked(View v, int position, String type);
    }
}