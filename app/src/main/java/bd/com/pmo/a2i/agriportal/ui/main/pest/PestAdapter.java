package bd.com.pmo.a2i.agriportal.ui.main.pest;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.model.Pest;
import bd.com.pmo.a2i.agriportal.ui.base.BaseViewHolder;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by JahangirKabir on 18-10-2017.
 */

public class PestAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private Callback mCallback;
    private List<Pest> pestList;

    public PestAdapter(List<Pest> pestList) {
        this.pestList = pestList;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pest, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (pestList != null && pestList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (pestList != null && pestList.size() > 0) {
            return pestList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<Pest> itemList) {
        pestList.addAll(itemList);
        notifyDataSetChanged();
    }

    public interface Callback {
        void onPestEmptyViewRetryClick();
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.cropNameTextView) TextView cropNameTextView;
        @BindView(R.id.diseaseNameTextView) TextView diseaseNameTextView;
        @BindView(R.id.itemImageView) ImageView itemImageView;
        @BindView(R.id.deleteImageView) ImageView deleteImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void clear() {
//            coverImageView.setImageDrawable(null);
            cropNameTextView.setText("");
            diseaseNameTextView.setText("");
        }

        public void onBind(int position) {
            super.onBind(position);

            final Pest pest = pestList.get(position);

//            if (blog.getCoverImgUrl() != null) {
//                Glide.with(itemView.getContext())
//                        .load(blog.getCoverImgUrl())
//                        .asBitmap()
//                        .centerCrop()
//                        .into(coverImageView);
//            }
//
//            if (blog.getTitle() != null) {
//                titleTextView.setText(blog.getTitle());
//            }
//
//            if (blog.getAuthor() != null) {
//                authorTextView.setText(blog.getAuthor());
//            }
//
//            if (blog.getDate() != null) {
//                dateTextView.setText(blog.getDate());
//            }
//
//            if (blog.getDescription() != null) {
//                contentTextView.setText(blog.getDescription());
//            }

//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (blog.getBlogUrl() != null) {
//                        try {
//                            Intent intent = new Intent();
//                            intent.setAction(Intent.ACTION_VIEW);
//                            intent.addCategory(Intent.CATEGORY_BROWSABLE);
//                            intent.setData(Uri.parse(blog.getBlogUrl()));
//                            itemView.getContext().startActivity(intent);
//                        } catch (Exception e) {
//                            AppLogger.d("url error");
//                        }
//                    }
//                }
//            });

            cropNameTextView.setText(pest.getCropName());
            diseaseNameTextView.setText(pest.getDiseaseName());

        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
            if (mCallback != null)
                mCallback.onPestEmptyViewRetryClick();
        }
    }
}
