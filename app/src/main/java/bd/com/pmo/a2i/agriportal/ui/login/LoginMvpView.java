package bd.com.pmo.a2i.agriportal.ui.login;

import bd.com.pmo.a2i.agriportal.ui.base.MvpView;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public interface LoginMvpView extends MvpView {

    void openMainActivity();
}
