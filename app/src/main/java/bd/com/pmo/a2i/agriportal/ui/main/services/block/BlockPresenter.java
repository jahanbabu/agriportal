package bd.com.pmo.a2i.agriportal.ui.main.services.block;

import com.androidnetworking.error.ANError;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.data.DataManager;
import bd.com.pmo.a2i.agriportal.data.network.model.LoginRequest;
import bd.com.pmo.a2i.agriportal.data.network.model.LoginResponse;
import bd.com.pmo.a2i.agriportal.model.Block;
import bd.com.pmo.a2i.agriportal.ui.base.BasePresenter;
import bd.com.pmo.a2i.agriportal.ui.login.LoginMvpPresenter;
import bd.com.pmo.a2i.agriportal.ui.login.LoginMvpView;
import bd.com.pmo.a2i.agriportal.utils.CommonUtils;
import bd.com.pmo.a2i.agriportal.utils.rx.SchedulerProvider;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public class BlockPresenter<V extends BlockMvpView> extends BasePresenter<V>
        implements BlockMvpPresenter<V> {

    private static final String TAG = "HatDetailsPresenter";
    private List<Block> blockList;

    @Inject
    public BlockPresenter(DataManager dataManager,
                          SchedulerProvider schedulerProvider,
                          CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    private void prepareData() {
        blockList = new ArrayList<>();
        for (int i = 0; i < 25; i++){
            Block item = new Block(i+"", "block_name"+" - "+i, "union_name"+" - "+i, "officer_name"+" - "+i, "mobile_number"+" - "+i);
            blockList.add(item);
        }
        getMvpView().updateBlock(blockList);
    }

    @Override
    public void onViewPrepared() {
        prepareData();

    }
}
