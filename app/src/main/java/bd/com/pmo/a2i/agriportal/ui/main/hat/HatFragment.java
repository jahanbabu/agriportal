package bd.com.pmo.a2i.agriportal.ui.main.hat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.di.component.ActivityComponent;
import bd.com.pmo.a2i.agriportal.model.Hat;
import bd.com.pmo.a2i.agriportal.ui.base.BaseFragment;
import bd.com.pmo.a2i.agriportal.ui.main.hat.hatDetails.HatDetailsActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public class HatFragment extends BaseFragment implements
        HatMvpView, HatAdapter.Callback {

    private static final String TAG = "HatFragment";

    @Inject
    HatMvpPresenter<HatMvpView> mPresenter;

    @Inject
    HatAdapter mHatAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @BindView(R.id.hat_recycler_view)
    RecyclerView mRecyclerView;

    public static HatFragment newInstance() {
        Bundle args = new Bundle();
        HatFragment fragment = new HatFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hat, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
            mHatAdapter.setCallback(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mHatAdapter);

        mPresenter.onViewPrepared();
    }

    @Override
    public void onHatEmptyViewRetryClick() {

    }

    @Override
    public void onHatItemViewClick(int position) {
        startActivity(new Intent(getContext(), HatDetailsActivity.class));
    }

    @Override
    public void updateHat(List<Hat> blogList) {
        mHatAdapter.addItems(blogList);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

}
