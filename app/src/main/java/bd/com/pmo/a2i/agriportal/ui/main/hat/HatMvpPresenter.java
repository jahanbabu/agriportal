package bd.com.pmo.a2i.agriportal.ui.main.hat;

import bd.com.pmo.a2i.agriportal.ui.base.MvpPresenter;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public interface HatMvpPresenter<V extends HatMvpView>
        extends MvpPresenter<V> {

    void onViewPrepared();
}


