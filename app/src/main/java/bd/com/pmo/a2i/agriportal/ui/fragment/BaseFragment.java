package bd.com.pmo.a2i.agriportal.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wang.avi.AVLoadingIndicatorView;

import bd.com.pmo.a2i.agriportal.R;
import butterknife.BindView;

public class BaseFragment extends Fragment {
    @BindView(R.id.loadingTextView) TextView loadingTextView;
    @BindView(R.id.avi) AVLoadingIndicatorView avi;
    @BindView(R.id.loadingLayout) LinearLayout loadingLayout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void showProgressDialog(String message) {
        loadingLayout.setVisibility(View.VISIBLE);
        avi.show();
        loadingTextView.setText(message);
    }

    protected void hideProgressDialog() {
        loadingLayout.setVisibility(View.GONE);
    }
}
