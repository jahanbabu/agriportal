package bd.com.pmo.a2i.agriportal.ui.main.variety;

import bd.com.pmo.a2i.agriportal.ui.base.MvpPresenter;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public interface VarietyMvpPresenter<V extends VarietyMvpView>
        extends MvpPresenter<V> {

    void onViewPrepared();
}


