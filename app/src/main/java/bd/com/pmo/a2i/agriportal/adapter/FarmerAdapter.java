package bd.com.pmo.a2i.agriportal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.model.Farmer;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FarmerAdapter extends RecyclerView.Adapter<FarmerAdapter.ViewHolder> {
    private ArrayList<Farmer> farmers;
    private Context context;
    private FarmerClickListener farmerClickListener;

    static class ViewHolder extends RecyclerView.ViewHolder{
        private Context context;
        // each data item is just a string in this case
        @BindView(R.id.farmerNameTextView) TextView farmerNameTextView;
        @BindView(R.id.farmersFatherNameTextView) TextView farmersFatherNameTextView;
        @BindView(R.id.farmerNumberTextView) TextView farmerNumberTextView;
        @BindView(R.id.farmerTypeTextView) TextView farmerTypeTextView;
        @BindView(R.id.genderTextView) TextView genderTextView;
        @BindView(R.id.nidTextView) TextView nidTextView;
        @BindView(R.id.cardNoTextView) TextView cardNoTextView;
        @BindView(R.id.blockNameTextView) TextView blockNameTextView;
        @BindView(R.id.areaNameTextView) TextView areaNameTextView;
        @BindView(R.id.villageNameTextView) TextView villageNameTextView;
        @BindView(R.id.deleteImageView) ImageView deleteImageView;

        View view;
        ViewHolder(Context context, View v) {
            super(v);
            this.context = context;
            this.view = v;
            // 2. Bind Butter knife to this view holder
            ButterKnife.bind(this, itemView);
        }
    }

    public FarmerAdapter(Context context, ArrayList<Farmer> farmers) {
        this.farmers = farmers;
        this.context = context;
    }

    @Override
    public FarmerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_farmer, parent, false);

        return new ViewHolder(context, v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.farmerNameTextView.setText(farmers.get(position).getFarmerName());
        holder.farmersFatherNameTextView.setText(farmers.get(position).getFarmerFatherName());
        holder.farmerNumberTextView.setText(farmers.get(position).getFarmerNumber());
        holder.farmerTypeTextView.setText(farmers.get(position).getFarmerType());
        holder.genderTextView.setText(farmers.get(position).getGender());
        holder.cardNoTextView.setText(farmers.get(position).getCardNumber());
        holder.nidTextView.setText(farmers.get(position).getNidNumber());
        holder.blockNameTextView.setText(farmers.get(position).getBlockName());
        holder.areaNameTextView.setText(farmers.get(position).getAreaName());
        holder.villageNameTextView.setText(farmers.get(position).getVillageName());

        holder.view.setOnClickListener(view -> farmerClickListener.blockItemClicked(view, position, "row"));
        holder.deleteImageView.setOnClickListener(view -> farmerClickListener.blockItemClicked(view, position, "delete"));
    }

    @Override
    public int getItemCount() {
        return farmers.size();
    }

    public void setClickListener(FarmerClickListener clickListener) {
        this.farmerClickListener = clickListener;
    }

    public interface FarmerClickListener {
        void blockItemClicked(View v, int position, String type);
    }
}