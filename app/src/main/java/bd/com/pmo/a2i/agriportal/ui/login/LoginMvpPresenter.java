package bd.com.pmo.a2i.agriportal.ui.login;


import bd.com.pmo.a2i.agriportal.di.PerActivity;
import bd.com.pmo.a2i.agriportal.ui.base.MvpPresenter;

/**
 * Created by JahangirKabir on 18/10/17.
 */

@PerActivity
public interface LoginMvpPresenter<V extends LoginMvpView> extends MvpPresenter<V> {

    void onServerLoginClick(String email, String password);

    void onGoogleLoginClick();

    void onFacebookLoginClick();

}
