package bd.com.pmo.a2i.agriportal.model;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public class Variety {
    String vid, crop, name, localName, instituteName;

    public Variety(String vid, String crop, String name, String localName, String instituteName) {
        this.vid = vid;
        this.crop = crop;
        this.name = name;
        this.localName = localName;
        this.instituteName = instituteName;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getCrop() {
        return crop;
    }

    public void setCrop(String crop) {
        this.crop = crop;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    @Override
    public String toString() {
        return "Variety{" +
                "vid='" + vid + '\'' +
                ", crop='" + crop + '\'' +
                ", name='" + name + '\'' +
                ", localName='" + localName + '\'' +
                ", instituteName='" + instituteName + '\'' +
                '}';
    }
}
