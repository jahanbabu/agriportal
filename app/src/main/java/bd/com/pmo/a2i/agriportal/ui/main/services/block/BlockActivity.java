package bd.com.pmo.a2i.agriportal.ui.main.services.block;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import javax.inject.Inject;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.model.Block;
import bd.com.pmo.a2i.agriportal.ui.base.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public class BlockActivity extends BaseActivity implements BlockMvpView, BlockAdapter.BlockClickListener{

    @Inject
    BlockMvpPresenter<BlockMvpView> mPresenter;

    @Inject
    BlockAdapter mBlockAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @BindView(R.id.recyclerView) RecyclerView mRecyclerView;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, BlockActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_only_recyclerview);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(BlockActivity.this);

        mBlockAdapter.setClickListener(this);

        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mBlockAdapter);

        getSupportActionBar().setTitle(getResources().getString(R.string.block));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mPresenter.onViewPrepared();
    }

    @Override
    public void blockItemClicked(View v, int position, String type) {

    }

    @Override
    public void updateBlock(List<Block> blockList) {
        mBlockAdapter.addItems(blockList);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
