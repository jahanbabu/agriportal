package bd.com.pmo.a2i.agriportal.ui.main.notification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;
import javax.inject.Inject;
import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.di.component.ActivityComponent;
import bd.com.pmo.a2i.agriportal.model.Notification;
import bd.com.pmo.a2i.agriportal.ui.base.BaseFragment;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public class NotificationFragment extends BaseFragment implements NotificationMvpView, NotificationAdapter.Callback {

    private static final String TAG = "VarietyFragment";

    @Inject
    NotificationMvpPresenter<NotificationMvpView> mPresenter;

    @Inject
    NotificationAdapter notificationAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @BindView(R.id.hat_recycler_view)
    RecyclerView mRecyclerView;

    public static NotificationFragment newInstance() {
        Bundle args = new Bundle();
        NotificationFragment fragment = new NotificationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hat, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
            notificationAdapter.setCallback(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(notificationAdapter);

        mPresenter.onViewPrepared();
    }

    @Override
    public void onNotificationEmptyViewRetryClick() {

    }

    @Override
    public void updateNotification(List<Notification> itemList) {
        notificationAdapter.addItems(itemList);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

}
