package bd.com.pmo.a2i.agriportal.model;

/**
 * Created by JK on 10/07/17.
 */

public class Farmer {
    String  farmerName, farmerFatherName, farmerNumber, nidNumber, gender, blockName, areaName, villageName, cardNumber, farmerType;

    public Farmer(String farmerName, String farmerFatherName, String farmerNumber, String nidNumber, String gender, String blockName, String areaName, String villageName, String cardNumber, String farmerType) {
        this.farmerName = farmerName;
        this.farmerFatherName = farmerFatherName;
        this.farmerNumber = farmerNumber;
        this.nidNumber = nidNumber;
        this.gender = gender;
        this.blockName = blockName;
        this.areaName = areaName;
        this.villageName = villageName;
        this.cardNumber = cardNumber;
        this.farmerType = farmerType;
    }

    @Override
    public String toString() {
        return "Farmer{" +
                "farmerName='" + farmerName + '\'' +
                ", farmerFatherName='" + farmerFatherName + '\'' +
                ", farmerNumber='" + farmerNumber + '\'' +
                ", nidNumber='" + nidNumber + '\'' +
                ", gender='" + gender + '\'' +
                ", blockName='" + blockName + '\'' +
                ", areaName='" + areaName + '\'' +
                ", villageName='" + villageName + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                ", farmerType='" + farmerType + '\'' +
                '}';
    }

    public String getFarmerName() {
        return farmerName;
    }

    public void setFarmerName(String farmerName) {
        this.farmerName = farmerName;
    }

    public String getFarmerFatherName() {
        return farmerFatherName;
    }

    public void setFarmerFatherName(String farmerFatherName) {
        this.farmerFatherName = farmerFatherName;
    }

    public String getFarmerNumber() {
        return farmerNumber;
    }

    public void setFarmerNumber(String farmerNumber) {
        this.farmerNumber = farmerNumber;
    }

    public String getNidNumber() {
        return nidNumber;
    }

    public void setNidNumber(String nidNumber) {
        this.nidNumber = nidNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getFarmerType() {
        return farmerType;
    }

    public void setFarmerType(String farmerType) {
        this.farmerType = farmerType;
    }
}
