package bd.com.pmo.a2i.agriportal.ui.main.pest;

import java.util.List;

import bd.com.pmo.a2i.agriportal.model.Hat;
import bd.com.pmo.a2i.agriportal.model.Pest;
import bd.com.pmo.a2i.agriportal.ui.base.MvpView;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public interface PestMvpView extends MvpView {

    void updatePest(List<Pest> pestList);
}
