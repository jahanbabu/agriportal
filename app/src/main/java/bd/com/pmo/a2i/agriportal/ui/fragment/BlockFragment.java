package bd.com.pmo.a2i.agriportal.ui.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.ui.main.services.block.BlockAdapter;
import bd.com.pmo.a2i.agriportal.model.Block;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BlockFragment extends BaseFragment implements BlockAdapter.BlockClickListener{
    private OnFragmentInteractionListener mListener;
    private BlockAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private String TAG = BlockFragment.class.getSimpleName();
    private ArrayList<Block> items = new ArrayList<>();
    private ActionBar actionBar;
    @BindView(R.id.recyclerView) RecyclerView mRecyclerView;
//    @BindView(R.id.loadingTextView) TextView loadingTextView;
//    @BindView(R.id.avi) AVLoadingIndicatorView avi;
//    @BindView(R.id.loadingLayout) LinearLayout loadingLayout;

    public BlockFragment() {
        // Required empty public constructor
    }

    public static BlockFragment newInstance(String param1, String param2) {
        BlockFragment fragment = new BlockFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    public static BlockFragment newInstance() {
        BlockFragment fragment = new BlockFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_only_recyclerview, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        prepareData();
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new BlockAdapter(items);
        mAdapter.setClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void prepareData() {
        for (int i = 0; i < 25; i++){
            Block item = new Block(i+"", getResources().getString(R.string.block_name)+" - "+i, getResources().getString(R.string.union_name)+" - "+i, getResources().getString(R.string.officer_name)+" - "+i, getResources().getString(R.string.mobile_number)+""+i);
            items.add(item);
        }
    }

    @Override
    public void blockItemClicked(View v, int position, String type) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
