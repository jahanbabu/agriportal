package bd.com.pmo.a2i.agriportal.ui.main.services.block;

import java.util.List;
import bd.com.pmo.a2i.agriportal.model.Block;
import bd.com.pmo.a2i.agriportal.ui.base.MvpView;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public interface BlockMvpView extends MvpView {

    void updateBlock(List<Block> blockList);
}
