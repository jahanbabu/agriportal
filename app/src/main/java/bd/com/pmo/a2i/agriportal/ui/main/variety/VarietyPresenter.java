package bd.com.pmo.a2i.agriportal.ui.main.variety;

import java.util.ArrayList;

import javax.inject.Inject;

import bd.com.pmo.a2i.agriportal.data.DataManager;
import bd.com.pmo.a2i.agriportal.model.Notification;
import bd.com.pmo.a2i.agriportal.model.Variety;
import bd.com.pmo.a2i.agriportal.ui.base.BasePresenter;
import bd.com.pmo.a2i.agriportal.utils.rx.SchedulerProvider;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public class VarietyPresenter<V extends VarietyMvpView> extends BasePresenter<V>
        implements VarietyMvpPresenter<V> {

    @Inject
    public VarietyPresenter(DataManager dataManager,
                            SchedulerProvider schedulerProvider,
                            CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
        prepareData();
        getMvpView().updateVariety(varieties);
//        getMvpView().showLoading();
//        getCompositeDisposable().add(getDataManager()
//                .getBlogApiCall()
//                .subscribeOn(getSchedulerProvider().io())
//                .observeOn(getSchedulerProvider().ui())
//                .subscribe(new Consumer<BlogResponse>() {
//                    @Override
//                    public void accept(@NonNull BlogResponse blogResponse)
//                            throws Exception {
//                        if (blogResponse != null && blogResponse.getData() != null) {
//                            getMvpView().updateHat(blogResponse.getData());
//                        }
//                        getMvpView().hideLoading();
//                    }
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(@NonNull Throwable throwable)
//                            throws Exception {
//                        if (!isViewAttached()) {
//                            return;
//                        }
//
//                        getMvpView().hideLoading();
//
//                        // handle the error here
//                        if (throwable instanceof ANError) {
//                            ANError anError = (ANError) throwable;
//                            handleApiError(anError);
//                        }
//                    }
//                }));
    }
    ArrayList<Variety> varieties = new ArrayList<>();
    private void prepareData() {
        for (int i = 0; i < 25; i++){
            Variety h = new Variety(i+"", "ফসল"+" - "+i, "নাম"+" - "+i, "আঞ্চলিক নাম", "অবমুক্তকারী প্রতিষ্ঠান" + " - "+i);
            varieties.add(h);
        }
    }
}
