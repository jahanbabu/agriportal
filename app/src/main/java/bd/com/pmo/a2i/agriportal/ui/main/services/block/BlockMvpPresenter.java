package bd.com.pmo.a2i.agriportal.ui.main.services.block;

import bd.com.pmo.a2i.agriportal.di.PerActivity;
import bd.com.pmo.a2i.agriportal.ui.base.MvpPresenter;

/**
 * Created by JahangirKabir on 18/10/17.
 */

@PerActivity
public interface BlockMvpPresenter<V extends BlockMvpView> extends MvpPresenter<V> {

    void onViewPrepared();

}
