package bd.com.pmo.a2i.agriportal.ui.main.hat;

import java.util.ArrayList;

import javax.inject.Inject;

import bd.com.pmo.a2i.agriportal.data.DataManager;
import bd.com.pmo.a2i.agriportal.model.Hat;
import bd.com.pmo.a2i.agriportal.ui.base.BasePresenter;
import bd.com.pmo.a2i.agriportal.utils.rx.SchedulerProvider;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public class HatPresenter<V extends HatMvpView> extends BasePresenter<V>
        implements HatMvpPresenter<V> {

    @Inject
    public HatPresenter(DataManager dataManager,
                        SchedulerProvider schedulerProvider,
                        CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
        prepareData();
        getMvpView().updateHat(hats);
//        getMvpView().showLoading();
//        getCompositeDisposable().add(getDataManager()
//                .getBlogApiCall()
//                .subscribeOn(getSchedulerProvider().io())
//                .observeOn(getSchedulerProvider().ui())
//                .subscribe(new Consumer<BlogResponse>() {
//                    @Override
//                    public void accept(@NonNull BlogResponse blogResponse)
//                            throws Exception {
//                        if (blogResponse != null && blogResponse.getData() != null) {
//                            getMvpView().updateHat(blogResponse.getData());
//                        }
//                        getMvpView().hideLoading();
//                    }
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(@NonNull Throwable throwable)
//                            throws Exception {
//                        if (!isViewAttached()) {
//                            return;
//                        }
//
//                        getMvpView().hideLoading();
//
//                        // handle the error here
//                        if (throwable instanceof ANError) {
//                            ANError anError = (ANError) throwable;
//                            handleApiError(anError);
//                        }
//                    }
//                }));
    }
    ArrayList<Hat> hats = new ArrayList<>();
    private void prepareData() {
        for (int i = 0; i < 25; i++){
            Hat h = new Hat(i+"", "বাজারের নাম"+" - "+i, "বাজারের ঠিকানা"+" - "+i, "", "১২-০৯-২০১৭ সকাল ৮টা-১২টা" + " - "+i);
            hats.add(h);
        }
    }
}
