package bd.com.pmo.a2i.agriportal.sip;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.rtp.AudioCodec;
import android.net.rtp.AudioGroup;
import android.net.rtp.AudioStream;
import android.net.rtp.RtpStream;
import android.net.sip.*;
import android.util.Log;

import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Listens for incoming SIP calls, intercepts and hands them off to WalkieTalkieActivity.
 */
public class IncomingCallReceiver extends BroadcastReceiver {
    String TAG = IncomingCallReceiver.class.getSimpleName();
    /**
     * Processes the incoming call, answers it, and hands it over to the
     * WalkieTalkieActivity.
     * @param context The context under which the receiver is running.
     * @param intent The intent being received.
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        SipAudioCall incomingCall = null;
        try {
            SipAudioCall.Listener listener = new SipAudioCall.Listener() {

                @Override
                public void onRinging(SipAudioCall call, SipProfile caller) {
                    try {
                        call.answerCall(30);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onCallEstablished(SipAudioCall call) {
                    Log.e(TAG, "onCallEstablished");
                    call.startAudio();
                    call.setSpeakerMode(true);
                    call.toggleMute();
//                    wtActivity.updateStatus(call);


                    AudioGroup audioGroup = new AudioGroup();
                    audioGroup.setMode(AudioGroup.MODE_NORMAL);
                    AudioStream audioStream = null;
                    try {
                        audioStream = new AudioStream(InetAddress.getByName("27.147.142.173"));
                    } catch (SocketException e) {
                        e.printStackTrace();
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }
//                    Log.e(TAG, audioStream.getCodec().toString() + "\n " +
//                            audioStream.getCodec().fmtp + "\n" +
//                            audioStream.getCodec().rtpmap);


                    audioStream.setCodec(AudioCodec.AMR);
                    audioStream.setMode(RtpStream.MODE_NORMAL);
                    try {
                        audioStream.associate(InetAddress.getByName(call.getPeerProfile().getSipDomain()), 5060);
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }
                    audioStream.join(audioGroup);
                    AudioManager Audio =  (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                    Audio.setMode(AudioManager.MODE_IN_COMMUNICATION);

                }

                @Override
                public void onCallEnded(SipAudioCall call) {
                    Log.e(TAG, "onCallEnded");
                }
            };
            WalkieTalkieActivity wtActivity = (WalkieTalkieActivity) context;
            incomingCall = wtActivity.manager.takeAudioCall(intent, listener);
            incomingCall.answerCall(30);
            incomingCall.startAudio();
            incomingCall.setSpeakerMode(true);
            if(incomingCall.isMuted()) {
                incomingCall.toggleMute();
            }
            wtActivity.call = incomingCall;
            wtActivity.updateStatus(incomingCall);
        } catch (Exception e) {
            if (incomingCall != null) {
                incomingCall.close();
            }
        }
    }
}