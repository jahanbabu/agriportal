package bd.com.pmo.a2i.agriportal.ui.main.hat.hatDetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.model.HatItem;
import bd.com.pmo.a2i.agriportal.ui.base.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public class HatDetailsActivity extends BaseActivity implements HatDetailsMvpView, HatDetailsAdapter.ItemClickListener{

    @Inject
    HatDetailsMvpPresenter<HatDetailsMvpView> mPresenter;

    @Inject
    HatDetailsAdapter mHatDetailsAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @BindView(R.id.recyclerView) RecyclerView mRecyclerView;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, HatDetailsActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_only_recyclerview);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(HatDetailsActivity.this);

        mHatDetailsAdapter.setClickListener(this);

        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mHatDetailsAdapter);

        getSupportActionBar().setTitle(getResources().getString(R.string.hat_bazar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mPresenter.onViewPrepared();
    }



    @Override
    public void updateHatItem(List<HatItem> hatItems) {
        mHatDetailsAdapter.addItems(hatItems);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_save:
//                getPostData(hatItems);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void itemClicked(View v, int position, String type) {
        if (type.equalsIgnoreCase("add")) {
            mHatDetailsAdapter.changePrice(position, true);
//            hatItems.get(position).setPrice((Integer.parseInt(hatItems.get(position).getPrice()) + 1) + "");
//            mAdapter.notifyDataSetChanged();
        } else if (type.equalsIgnoreCase("minus")) {
            mHatDetailsAdapter.changePrice(position, false);
//            hatItems.get(position).setPrice((Integer.parseInt(hatItems.get(position).getPrice()) - 1) + "");
//            mAdapter.notifyDataSetChanged();
        }
    }

    private JSONObject getPostData(ArrayList<HatItem> hatItems) {
        JSONObject itemObject = new JSONObject();
        JSONArray itemArray = new JSONArray();

        for (HatItem i : hatItems) {
            itemObject = new JSONObject();
            try {
                itemObject.put("id", i.getId());
                itemObject.put("name", i.getName());
                itemObject.put("price", i.getPrice());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            itemArray.put(itemObject);
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("hatItems", itemArray);

//            jsonObject.put("userId", uid);
//            jsonObject.put("hatId", hatID);
            Log.e("data", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
