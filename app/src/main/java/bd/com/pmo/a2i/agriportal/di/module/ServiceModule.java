package bd.com.pmo.a2i.agriportal.di.module;

import android.app.Service;

import dagger.Module;

/**
 * Created by JahangirKabir on 18/10/17.
 */

@Module
public class ServiceModule {

    private final Service mService;

    public ServiceModule(Service service) {
        mService = service;
    }
}
