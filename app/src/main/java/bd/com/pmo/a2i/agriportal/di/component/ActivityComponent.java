package bd.com.pmo.a2i.agriportal.di.component;

import bd.com.pmo.a2i.agriportal.di.PerActivity;
import bd.com.pmo.a2i.agriportal.di.module.ActivityModule;
import bd.com.pmo.a2i.agriportal.ui.login.LoginActivity;
import bd.com.pmo.a2i.agriportal.ui.main.MainActivity;
import bd.com.pmo.a2i.agriportal.ui.main.hat.HatFragment;
import bd.com.pmo.a2i.agriportal.ui.main.hat.hatDetails.HatDetailsActivity;
import bd.com.pmo.a2i.agriportal.ui.main.notification.NotificationFragment;
import bd.com.pmo.a2i.agriportal.ui.main.pest.PestFragment;
import bd.com.pmo.a2i.agriportal.ui.main.services.block.BlockActivity;
import bd.com.pmo.a2i.agriportal.ui.main.variety.VarietyFragment;
import bd.com.pmo.a2i.agriportal.ui.splash.SplashActivity;
import dagger.Component;

/**
 * Created by JahangirKabir on 18/10/17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity activity);
//
    void inject(LoginActivity activity);
//
    void inject(SplashActivity activity);

    void inject(BlockActivity activity);
//
//    void inject(AboutFragment fragment);
//
//    void inject(OpenSourceFragment fragment);
//
    void inject(HatFragment fragment);

    void inject(HatDetailsActivity activity);

    void inject(PestFragment fragment);

    void inject(VarietyFragment fragment);

    void inject(NotificationFragment fragment);

//    void inject(RateUsDialog dialog);

}
