package bd.com.pmo.a2i.agriportal.model;

/**
 * Created by JK on 10/16/17.
 */

public class Pest {
    String  cropName, diseaseName;

    public Pest(String cropName, String diseaseName) {
        this.cropName = cropName;
        this.diseaseName = diseaseName;
    }

    @Override
    public String toString() {
        return "Pest{" +
                "cropName='" + cropName + '\'' +
                ", diseaseName='" + diseaseName + '\'' +
                '}';
    }

    public String getCropName() {
        return cropName;
    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    public String getDiseaseName() {
        return diseaseName;
    }

    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }
}
