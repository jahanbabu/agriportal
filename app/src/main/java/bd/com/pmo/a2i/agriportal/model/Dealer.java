package bd.com.pmo.a2i.agriportal.model;

/**
 * Created by JK on 10/14/17.
 */

public class Dealer {
    String dealerName, dealerAddress, dealerNumber;

    public Dealer(String dealerName, String dealerAddress, String dealerNumber) {
        this.dealerName = dealerName;
        this.dealerAddress = dealerAddress;
        this.dealerNumber = dealerNumber;
    }

    @Override
    public String toString() {
        return "Dealer{" +
                "dealerName='" + dealerName + '\'' +
                ", dealerAddress='" + dealerAddress + '\'' +
                ", dealerNumber='" + dealerNumber + '\'' +
                '}';
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerAddress() {
        return dealerAddress;
    }

    public void setDealerAddress(String dealerAddress) {
        this.dealerAddress = dealerAddress;
    }

    public String getDealerNumber() {
        return dealerNumber;
    }

    public void setDealerNumber(String dealerNumber) {
        this.dealerNumber = dealerNumber;
    }
}
