package bd.com.pmo.a2i.agriportal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.model.Pest;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PestAdapter extends RecyclerView.Adapter<PestAdapter.ViewHolder> {
    private ArrayList<Pest> pests;
    private Context context;
    private PestClickListener pestClickListener;

    static class ViewHolder extends RecyclerView.ViewHolder{
        private Context context;
        // each data item is just a string in this case
        @BindView(R.id.cropNameTextView) TextView cropNameTextView;
        @BindView(R.id.diseaseNameTextView) TextView diseaseNameTextView;
        @BindView(R.id.imageview) ImageView itemImageView;
        @BindView(R.id.deleteImageView) ImageView deleteImageView;

        View view;
        ViewHolder(Context context, View v) {
            super(v);
            this.context = context;
            this.view = v;
            // 2. Bind Butter knife to this view holder
            ButterKnife.bind(this, itemView);
        }
    }

    public PestAdapter(Context context, ArrayList<Pest> pests) {
        this.pests = pests;
        this.context = context;
    }

    @Override
    public PestAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_pest, parent, false);

        return new ViewHolder(context, v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.cropNameTextView.setText(pests.get(position).getCropName());
        holder.diseaseNameTextView.setText(pests.get(position).getDiseaseName());

        holder.view.setOnClickListener(view -> pestClickListener.blockItemClicked(view, position, "row"));
        holder.deleteImageView.setOnClickListener(view -> pestClickListener.blockItemClicked(view, position, "delete"));
    }

    @Override
    public int getItemCount() {
        return pests.size();
    }

    public void setClickListener(PestClickListener clickListener) {
        this.pestClickListener = clickListener;
    }

    public interface PestClickListener {
        void blockItemClicked(View v, int position, String type);
    }
}