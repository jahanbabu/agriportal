package bd.com.pmo.a2i.agriportal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.model.Hat;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HatAdapter extends RecyclerView.Adapter<HatAdapter.ViewHolder> {
    private ArrayList<Hat> hats;
    private Context context;
    private HatClickListener hatClickListener;

    static class ViewHolder extends RecyclerView.ViewHolder{
        private Context context;
        // each data item is just a string in this case
        @BindView(R.id.hatNameTextView) TextView hatNameTextView;
        @BindView(R.id.hatAddressTextView) TextView hatAddressTextView;
        @BindView(R.id.hatTimeTextView) TextView hatTimeTextView;
        @BindView(R.id.deleteImageView) ImageView deleteImageView;

        View view;
        ViewHolder(Context context, View v) {
            super(v);
            this.context = context;
            this.view = v;
            // 2. Bind Butter knife to this view holder
            ButterKnife.bind(this, itemView);
        }
    }

    public HatAdapter(Context context, ArrayList<Hat> hats) {
        this.hats = hats;
        this.context = context;
    }

    @Override
    public HatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_hat_bazar, parent, false);

        return new ViewHolder(context, v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

//        if (hats.get(position).getStartAddress() == null )
//            holder.startAddressTextView.setText("Select Doctor");

        holder.hatNameTextView.setText(hats.get(position).getHatName());
        holder.hatAddressTextView.setText(hats.get(position).getHatAddress());
        holder.hatTimeTextView.setText(hats.get(position).getHatTime());

        holder.view.setOnClickListener(view -> hatClickListener.hatItemClicked(view, position, "row"));
    }

    @Override
    public int getItemCount() {
        return hats.size();
    }

    public void setClickListener(HatClickListener clickListener) {
        this.hatClickListener = clickListener;
    }

    public interface HatClickListener {
        void hatItemClicked(View v, int position, String type);
    }
}