package bd.com.pmo.a2i.agriportal.ui.main;

import bd.com.pmo.a2i.agriportal.ui.base.MvpPresenter;
import bd.com.pmo.a2i.agriportal.ui.base.MvpView;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public interface MainMvpPresenter<V extends MvpView> extends MvpPresenter<V> {

}