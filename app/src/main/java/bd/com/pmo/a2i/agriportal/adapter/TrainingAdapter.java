package bd.com.pmo.a2i.agriportal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.model.Training;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TrainingAdapter extends RecyclerView.Adapter<TrainingAdapter.ViewHolder> {
    private ArrayList<Training> trainings;
    private Context context;
    private TrainingClickListener trainingClickListener;

    static class ViewHolder extends RecyclerView.ViewHolder{
        private Context context;
        // each data item is just a string in this case
        @BindView(R.id.trainingNameTextView) TextView trainingNameTextView;
        @BindView(R.id.trainingHeadTextView) TextView trainingHeadTextView;
        @BindView(R.id.perticipentNumberTextView) TextView perticipentNumberTextView;
        @BindView(R.id.userTextView) TextView userTextView;

        View view;
        ViewHolder(Context context, View v) {
            super(v);
            this.context = context;
            this.view = v;
            // 2. Bind Butter knife to this view holder
            ButterKnife.bind(this, itemView);
        }
    }

    public TrainingAdapter(Context context, ArrayList<Training> trainings) {
        this.trainings = trainings;
        this.context = context;
    }

    @Override
    public TrainingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_training, parent, false);

        return new ViewHolder(context, v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.trainingNameTextView.setText(trainings.get(position).getTrainingName());
        holder.trainingHeadTextView.setText(trainings.get(position).getTrainingHeadName());
        holder.perticipentNumberTextView.setText(trainings.get(position).getPerticipentNumber());
        holder.userTextView.setText(trainings.get(position).getUserName());

        holder.view.setOnClickListener(view -> trainingClickListener.blockItemClicked(view, position, "row"));
//        holder.deleteImageView.setOnClickListener(view -> trainingClickListener.blockItemClicked(view, position, "delete"));
    }

    @Override
    public int getItemCount() {
        return trainings.size();
    }

    public void setClickListener(TrainingClickListener clickListener) {
        this.trainingClickListener = clickListener;
    }

    public interface TrainingClickListener {
        void blockItemClicked(View v, int position, String type);
    }
}