package bd.com.pmo.a2i.agriportal.ui.main.variety;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.model.Variety;
import bd.com.pmo.a2i.agriportal.ui.base.BaseViewHolder;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by JahangirKabir on 18-10-2017.
 */

public class VarietyAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private Callback mCallback;
    private List<Variety> varietyList;

    public VarietyAdapter(List<Variety> varietyList) {
        this.varietyList = varietyList;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.row_variety, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (varietyList != null && varietyList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (varietyList != null && varietyList.size() > 0) {
            return varietyList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<Variety> itemList) {
        varietyList.addAll(itemList);
        notifyDataSetChanged();
    }

    public interface Callback {
        void onVarietyEmptyViewRetryClick();
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.cropTextView) TextView cropTextView;
        @BindView(R.id.nameTextView) TextView cropNameTextView;
        @BindView(R.id.localNameTextView) TextView localNameTextView;
        @BindView(R.id.instituteNameTextView) TextView instituteNameTextView;
        @BindView(R.id.itemImageView) ImageView itemImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void clear() {
//            coverImageView.setImageDrawable(null);
            cropTextView.setText("");
            cropNameTextView.setText("");
        }

        public void onBind(int position) {
            super.onBind(position);

            final Variety pest = varietyList.get(position);

//            if (blog.getCoverImgUrl() != null) {
//                Glide.with(itemView.getContext())
//                        .load(blog.getCoverImgUrl())
//                        .asBitmap()
//                        .centerCrop()
//                        .into(coverImageView);
//            }
//
//            if (blog.getTitle() != null) {
//                titleTextView.setText(blog.getTitle());
//            }
//
//            if (blog.getAuthor() != null) {
//                authorTextView.setText(blog.getAuthor());
//            }
//
//            if (blog.getDate() != null) {
//                dateTextView.setText(blog.getDate());
//            }
//
//            if (blog.getDescription() != null) {
//                contentTextView.setText(blog.getDescription());
//            }

//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (blog.getBlogUrl() != null) {
//                        try {
//                            Intent intent = new Intent();
//                            intent.setAction(Intent.ACTION_VIEW);
//                            intent.addCategory(Intent.CATEGORY_BROWSABLE);
//                            intent.setData(Uri.parse(blog.getBlogUrl()));
//                            itemView.getContext().startActivity(intent);
//                        } catch (Exception e) {
//                            AppLogger.d("url error");
//                        }
//                    }
//                }
//            });

            cropTextView.setText(pest.getCrop());
            cropNameTextView.setText(pest.getName());
            localNameTextView.setText(pest.getLocalName());
            instituteNameTextView.setText(pest.getInstituteName());

        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
            if (mCallback != null)
                mCallback.onVarietyEmptyViewRetryClick();
        }
    }
}
