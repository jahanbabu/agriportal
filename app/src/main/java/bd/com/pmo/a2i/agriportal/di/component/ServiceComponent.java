package bd.com.pmo.a2i.agriportal.di.component;

import bd.com.pmo.a2i.agriportal.di.PerService;
import bd.com.pmo.a2i.agriportal.di.module.ServiceModule;
import bd.com.pmo.a2i.agriportal.service.SyncService;
import dagger.Component;

/**
 * Created by JahangirKabir on 18/10/17.
 */

@PerService
@Component(dependencies = ApplicationComponent.class, modules = ServiceModule.class)
public interface ServiceComponent {

    void inject(SyncService service);

}
