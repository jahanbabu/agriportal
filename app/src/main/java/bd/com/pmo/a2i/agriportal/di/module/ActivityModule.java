package bd.com.pmo.a2i.agriportal.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import java.util.ArrayList;

import bd.com.pmo.a2i.agriportal.di.ActivityContext;
import bd.com.pmo.a2i.agriportal.di.PerActivity;
import bd.com.pmo.a2i.agriportal.model.Block;
import bd.com.pmo.a2i.agriportal.model.Hat;
import bd.com.pmo.a2i.agriportal.model.HatItem;
import bd.com.pmo.a2i.agriportal.model.Notification;
import bd.com.pmo.a2i.agriportal.model.Pest;
import bd.com.pmo.a2i.agriportal.model.Variety;
import bd.com.pmo.a2i.agriportal.ui.login.LoginMvpPresenter;
import bd.com.pmo.a2i.agriportal.ui.login.LoginMvpView;
import bd.com.pmo.a2i.agriportal.ui.login.LoginPresenter;
import bd.com.pmo.a2i.agriportal.ui.main.MainMvpPresenter;
import bd.com.pmo.a2i.agriportal.ui.main.MainMvpView;
import bd.com.pmo.a2i.agriportal.ui.main.MainPresenter;
import bd.com.pmo.a2i.agriportal.ui.main.hat.HatAdapter;
import bd.com.pmo.a2i.agriportal.ui.main.hat.HatMvpPresenter;
import bd.com.pmo.a2i.agriportal.ui.main.hat.HatMvpView;
import bd.com.pmo.a2i.agriportal.ui.main.hat.HatPresenter;
import bd.com.pmo.a2i.agriportal.ui.main.hat.hatDetails.HatDetailsMvpPresenter;
import bd.com.pmo.a2i.agriportal.ui.main.hat.hatDetails.HatDetailsMvpView;
import bd.com.pmo.a2i.agriportal.ui.main.hat.hatDetails.HatDetailsPresenter;
import bd.com.pmo.a2i.agriportal.ui.main.hat.hatDetails.HatDetailsAdapter;
import bd.com.pmo.a2i.agriportal.ui.main.notification.NotificationAdapter;
import bd.com.pmo.a2i.agriportal.ui.main.notification.NotificationMvpPresenter;
import bd.com.pmo.a2i.agriportal.ui.main.notification.NotificationMvpView;
import bd.com.pmo.a2i.agriportal.ui.main.notification.NotificationPresenter;
import bd.com.pmo.a2i.agriportal.ui.main.pest.PestAdapter;
import bd.com.pmo.a2i.agriportal.ui.main.pest.PestMvpPresenter;
import bd.com.pmo.a2i.agriportal.ui.main.pest.PestMvpView;
import bd.com.pmo.a2i.agriportal.ui.main.pest.PestPresenter;
import bd.com.pmo.a2i.agriportal.ui.main.services.block.BlockAdapter;
import bd.com.pmo.a2i.agriportal.ui.main.services.block.BlockMvpPresenter;
import bd.com.pmo.a2i.agriportal.ui.main.services.block.BlockMvpView;
import bd.com.pmo.a2i.agriportal.ui.main.services.block.BlockPresenter;
import bd.com.pmo.a2i.agriportal.ui.main.variety.VarietyAdapter;
import bd.com.pmo.a2i.agriportal.ui.main.variety.VarietyMvpPresenter;
import bd.com.pmo.a2i.agriportal.ui.main.variety.VarietyMvpView;
import bd.com.pmo.a2i.agriportal.ui.main.variety.VarietyPresenter;
import bd.com.pmo.a2i.agriportal.ui.splash.SplashMvpPresenter;
import bd.com.pmo.a2i.agriportal.ui.splash.SplashMvpView;
import bd.com.pmo.a2i.agriportal.ui.splash.SplashPresenter;
import bd.com.pmo.a2i.agriportal.utils.rx.AppSchedulerProvider;
import bd.com.pmo.a2i.agriportal.utils.rx.SchedulerProvider;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by JahangirKabir on 18/10/17.
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @PerActivity
    SplashMvpPresenter<SplashMvpView> provideSplashPresenter(
            SplashPresenter<SplashMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    LoginMvpPresenter<LoginMvpView> provideLoginPresenter(
            LoginPresenter<LoginMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> provideMainPresenter(
            MainPresenter<MainMvpView> presenter) {
        return presenter;
    }

    @Provides
    HatMvpPresenter<HatMvpView> provideHatMvpPresenter(
            HatPresenter<HatMvpView> presenter) {
        return presenter;
    }

    @Provides
    HatAdapter provideHatAdapter() {
        return new HatAdapter(new ArrayList<Hat>());
    }

    @Provides
    HatDetailsMvpPresenter<HatDetailsMvpView> provideHatDetailsMvpPresenter(
            HatDetailsPresenter<HatDetailsMvpView> presenter) {
        return presenter;
    }

    @Provides
    HatDetailsAdapter provideHatDetailsAdapter() {
        return new HatDetailsAdapter(new ArrayList<HatItem>());
    }

    @Provides
    PestMvpPresenter<PestMvpView> providePestMvpPresenter(
            PestPresenter<PestMvpView> presenter) {
        return presenter;
    }

    @Provides
    PestAdapter providePestAdapter() {
        return new PestAdapter(new ArrayList<Pest>());
    }

    @Provides
    VarietyMvpPresenter<VarietyMvpView> provideVarietyMvpPresenter(
            VarietyPresenter<VarietyMvpView> presenter) {
        return presenter;
    }

    @Provides
    VarietyAdapter provideVarietyAdapter() {
        return new VarietyAdapter(new ArrayList<Variety>());
    }

    @Provides
    NotificationMvpPresenter<NotificationMvpView> provideNotificationMvpPresenter(
            NotificationPresenter<NotificationMvpView> presenter) {
        return presenter;
    }

    @Provides
    NotificationAdapter provideNotificationAdapter() {
        return new NotificationAdapter(new ArrayList<Notification>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }

    //Apps Services Providers
    @Provides
    BlockMvpPresenter<BlockMvpView> provideBlockMvpPresenter(
            BlockPresenter<BlockMvpView> presenter) {
        return presenter;
    }

    @Provides
    BlockAdapter provideBlockAdapter() {
        return new BlockAdapter(new ArrayList<Block>());
    }
}
