package bd.com.pmo.a2i.agriportal.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Utility {

	public static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	public static boolean isNetworkAvailable(Context context){
		ConnectivityManager cm =
				(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean isConnected = activeNetwork != null &&
				activeNetwork.isConnectedOrConnecting();

		return isConnected;
	}

	public static void showNoInternetDialog(final Context context, final Activity activity) {
		AlertDialog dailog;
		AlertDialog.Builder build = new AlertDialog.Builder(context);
		build.setMessage("This application requires Internet connection.\nWould you connect to internet ?");
		build.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				activity.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
			}
		});
		build.setNegativeButton("No", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				AlertDialog dailog;
//				ContextThemeWrapper ctw = new ContextThemeWrapper(mContext, R.style.MyAlertDialogStyle);
				AlertDialog.Builder build = new AlertDialog.Builder(context);
				build.setMessage("Are sure you want to exit?");
				build.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						activity.finish();
					}
				});
				build.setNegativeButton("No", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				dailog = build.create();
				dailog.show();
			}
		});
		dailog = build.create();
		dailog.show();
	}

	public static void showAlertDialog(final Context context, String title, String msg) {
		AlertDialog dailog;
		AlertDialog.Builder build = new AlertDialog.Builder(context);
		build.setTitle(title);
		build.setMessage(msg);
		build.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		dailog = build.create();
		dailog.show();
	}

	public void showToast(Context context, String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
	}

	public void showToast(Context context, String msg, int duration) {
		Toast.makeText(context, msg, duration).show();
	}

	public void showMessage(Context context, String title, String message) {
		Builder builder = new Builder(context);
		builder.setCancelable(true);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.show();
	}


	public static String getRealPathFromURI(Context context, Uri contentUri) {
		Cursor cursor = null;
		try {
			String[] proj = { MediaStore.Images.Media.DATA };
			cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	public static String[] processCommaString(String s){
		return  s.split(",");
	}

	public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
		ActivityManager manager = (ActivityManager)context. getSystemService(Context.ACTIVITY_SERVICE);
		for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceClass.getName().equals(service.service.getClassName())) {
				Log.e("Service "," is running");
				return true;
			}
		}
		Log.e("Service "," not running");
		return false;
	}

	public static final String KEY_TotalSync = "TotalSync";
	public static final String KEY_TotalExpense = "TotalExpense";

	public static String getTotalSync(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(KEY_TotalSync, "0");
	}

	public static void setTotalSync(Context context, String s) {
		PreferenceManager.getDefaultSharedPreferences(context)
				.edit()
				.putString(KEY_TotalSync, s)
				.apply();
	}

	public static String getTotalExpense(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(KEY_TotalExpense, "0");
	}

	public static void setTotalExpense(Context context, String s) {
		PreferenceManager.getDefaultSharedPreferences(context)
				.edit()
				.putString(KEY_TotalExpense, s)
				.apply();
	}
}
