package bd.com.pmo.a2i.agriportal.di.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import bd.com.pmo.a2i.agriportal.MvpApp;
import bd.com.pmo.a2i.agriportal.data.DataManager;
import bd.com.pmo.a2i.agriportal.di.ApplicationContext;
import bd.com.pmo.a2i.agriportal.di.module.ApplicationModule;
import bd.com.pmo.a2i.agriportal.service.SyncService;
import dagger.Component;

/**
 * Created by JahangirKabir on 18/10/17.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(MvpApp app);

    void inject(SyncService service);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();
}