package bd.com.pmo.a2i.agriportal.ui.main;

import javax.inject.Inject;

import bd.com.pmo.a2i.agriportal.data.DataManager;
import bd.com.pmo.a2i.agriportal.ui.base.BasePresenter;
import bd.com.pmo.a2i.agriportal.ui.base.MvpView;
import bd.com.pmo.a2i.agriportal.utils.rx.SchedulerProvider;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public class MainPresenter<V extends MvpView> extends BasePresenter<V> implements
        MainMvpPresenter<V> {

    private static final String TAG = "MainPresenter";

    @Inject
    public MainPresenter(DataManager dataManager,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }
}
