package bd.com.pmo.a2i.agriportal.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.orhanobut.hawk.Hawk;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.adapter.ItemAdapter;
import bd.com.pmo.a2i.agriportal.model.HatItem;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemListActivity extends AppCompatActivity implements ItemAdapter.ItemClickListener {

//    private RecyclerView.Adapter mAdapter;
    private ItemAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    Context mContext;
    String TAG = ItemListActivity.class.getSimpleName();
    ArrayList<HatItem> hatItems = new ArrayList<>();
    ActionBar actionBar;
    String hatID, hatName, hatAddress;
    @BindView(R.id.recyclerView) RecyclerView mRecyclerView;
    String uid = "324131";

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hats);
        ButterKnife.bind(this);
        mContext = this;

        Hawk.init(mContext);

//        uid = Hawk.get("userId", "1234");

        hatID = getIntent().getStringExtra("hId");
        hatName = getIntent().getStringExtra("hName");
        hatAddress = getIntent().getStringExtra("hAddress");

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(hatName);

        prepareData();
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new ItemAdapter(this, hatItems);
        mAdapter.setClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
            case R.id.action_save:
                getPostData(hatItems);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void itemClicked(View v, int position, String type) {
        if (type.equalsIgnoreCase("add")) {
            hatItems.get(position).setPrice((Integer.parseInt(hatItems.get(position).getPrice()) + 1) + "");
            mAdapter.notifyDataSetChanged();
        } else if (type.equalsIgnoreCase("minus")) {
            hatItems.get(position).setPrice((Integer.parseInt(hatItems.get(position).getPrice()) - 1) + "");
            mAdapter.notifyDataSetChanged();
        }
    }

    private JSONObject getPostData(ArrayList<HatItem> hatItems) {
        JSONObject itemObject = new JSONObject();
        JSONArray itemArray = new JSONArray();

        for (HatItem i : hatItems) {
            itemObject = new JSONObject();
            try {
                itemObject.put("id", i.getId());
                itemObject.put("name", i.getName());
                itemObject.put("price", i.getPrice());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            itemArray.put(itemObject);
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("hatItems", itemArray);

            jsonObject.put("userId", uid);
            jsonObject.put("hatId", hatID);
            Log.e("data", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void prepareData() {
        for (int i = 0; i < 25; i++){
            HatItem hatItem = new HatItem(i+"", getResources().getString(R.string.fashol)+" - "+i, i+"");
            hatItems.add(hatItem);
        }
    }

}
