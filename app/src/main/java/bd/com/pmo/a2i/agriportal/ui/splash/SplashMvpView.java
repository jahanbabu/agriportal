package bd.com.pmo.a2i.agriportal.ui.splash;

import bd.com.pmo.a2i.agriportal.ui.base.MvpView;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public interface SplashMvpView extends MvpView {

    void openLoginActivity();

    void openMainActivity();

    void startSyncService();
}
