package bd.com.pmo.a2i.agriportal.ui.main.hat;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.model.Hat;
import bd.com.pmo.a2i.agriportal.ui.base.BaseViewHolder;
import bd.com.pmo.a2i.agriportal.ui.main.hat.hatDetails.HatDetailsActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by JahangirKabir on 18-10-2017.
 */

public class HatAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private Callback mCallback;
    private List<Hat> hatList;

    public HatAdapter(List<Hat> hatList) {
        this.hatList = hatList;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.row_hat_bazar, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (hatList != null && hatList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (hatList != null && hatList.size() > 0) {
            return hatList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<Hat> itemList) {
        hatList.addAll(itemList);
        notifyDataSetChanged();
    }

    public interface Callback {
        void onHatEmptyViewRetryClick();
        void onHatItemViewClick(int position);
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.hatNameTextView) TextView hatNameTextView;
        @BindView(R.id.hatAddressTextView) TextView hatAddressTextView;
        @BindView(R.id.hatTimeTextView) TextView hatTimeTextView;
        @BindView(R.id.deleteImageView) ImageView deleteImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void clear() {
//            coverImageView.setImageDrawable(null);
            hatNameTextView.setText("");
            hatAddressTextView.setText("");
        }

        public void onBind(int position) {
            super.onBind(position);

            final Hat hat = hatList.get(position);

//            if (blog.getCoverImgUrl() != null) {
//                Glide.with(itemView.getContext())
//                        .load(blog.getCoverImgUrl())
//                        .asBitmap()
//                        .centerCrop()
//                        .into(coverImageView);
//            }
//
//            if (blog.getTitle() != null) {
//                titleTextView.setText(blog.getTitle());
//            }
//
//            if (blog.getAuthor() != null) {
//                authorTextView.setText(blog.getAuthor());
//            }
//
//            if (blog.getDate() != null) {
//                dateTextView.setText(blog.getDate());
//            }
//
//            if (blog.getDescription() != null) {
//                contentTextView.setText(blog.getDescription());
//            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onHatItemViewClick(position);
                }
            });

            hatNameTextView.setText(hat.getHatName());
            hatAddressTextView.setText(hat.getHatAddress());
            hatTimeTextView.setText(hat.getHatTime());
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
            if (mCallback != null)
                mCallback.onHatEmptyViewRetryClick();
        }
    }
}
