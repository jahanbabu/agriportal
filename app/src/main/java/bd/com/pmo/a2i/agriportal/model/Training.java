package bd.com.pmo.a2i.agriportal.model;

/**
 * Created by JK on 10/14/17.
 */

public class Training {
    String  trainingName, trainingHeadName, perticipentNumber, userName;

    public Training(String trainingName, String trainingHeadName, String perticipentNumber, String userName) {
        this.trainingName = trainingName;
        this.trainingHeadName = trainingHeadName;
        this.perticipentNumber = perticipentNumber;
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "Training{" +
                "trainingName='" + trainingName + '\'' +
                ", trainingHeadName='" + trainingHeadName + '\'' +
                ", perticipentNumber='" + perticipentNumber + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }

    public String getTrainingName() {
        return trainingName;
    }

    public void setTrainingName(String trainingName) {
        this.trainingName = trainingName;
    }

    public String getTrainingHeadName() {
        return trainingHeadName;
    }

    public void setTrainingHeadName(String trainingHeadName) {
        this.trainingHeadName = trainingHeadName;
    }

    public String getPerticipentNumber() {
        return perticipentNumber;
    }

    public void setPerticipentNumber(String perticipentNumber) {
        this.perticipentNumber = perticipentNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
