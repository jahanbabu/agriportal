package bd.com.pmo.a2i.agriportal.ui.main.variety;

import java.util.List;

import bd.com.pmo.a2i.agriportal.model.Hat;
import bd.com.pmo.a2i.agriportal.model.Notification;
import bd.com.pmo.a2i.agriportal.model.Pest;
import bd.com.pmo.a2i.agriportal.model.Variety;
import bd.com.pmo.a2i.agriportal.ui.base.MvpView;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public interface VarietyMvpView extends MvpView {

    void updateVariety(List<Variety> varietyList);
}
