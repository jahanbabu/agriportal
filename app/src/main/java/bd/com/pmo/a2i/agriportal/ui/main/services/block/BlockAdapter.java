package bd.com.pmo.a2i.agriportal.ui.main.services.block;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.data.network.model.BlogResponse;
import bd.com.pmo.a2i.agriportal.model.Block;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BlockAdapter extends RecyclerView.Adapter<BlockAdapter.ViewHolder> {
    private ArrayList<Block> blocks;
    private Context context;
    private BlockClickListener blockClickListener;

    static class ViewHolder extends RecyclerView.ViewHolder{
        private Context context;
        // each data item is just a string in this case
        @BindView(R.id.blockNumberTextView) TextView blockNumberTextView;
        @BindView(R.id.blockNameTextView) TextView blockNameTextView;
        @BindView(R.id.blockUnionTextView) TextView blockUnionTextView;
        @BindView(R.id.officerNameTextView) TextView officerNameTextView;
        @BindView(R.id.officerNumberTextView) TextView officerNumberTextView;
        @BindView(R.id.deleteImageView) ImageView deleteImageView;

        View view;
        ViewHolder(Context context, View v) {
            super(v);
            this.context = context;
            this.view = v;
            // 2. Bind Butter knife to this view holder
            ButterKnife.bind(this, itemView);
        }
    }

    public BlockAdapter(ArrayList<Block> blocks) {
        this.blocks = blocks;
    }

    @Override
    public BlockAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_block, parent, false);

        return new ViewHolder(context, v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.blockNumberTextView.setText(blocks.get(position).getBlockNumber());
        holder.blockNameTextView.setText(blocks.get(position).getBlockName());
        holder.blockUnionTextView.setText(blocks.get(position).getUnion());
        holder.officerNameTextView.setText(blocks.get(position).getOfficerName());
        holder.officerNumberTextView.setText(blocks.get(position).getOfficerNumber());

        holder.view.setOnClickListener(view -> blockClickListener.blockItemClicked(view, position, "row"));
        holder.deleteImageView.setOnClickListener(view -> blockClickListener.blockItemClicked(view, position, "delete"));
    }

    public void addItems(List<Block> itemList) {
        blocks.addAll(itemList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return blocks.size();
    }

    public void setClickListener(BlockClickListener clickListener) {
        this.blockClickListener = clickListener;
    }

    public interface BlockClickListener {
        void blockItemClicked(View v, int position, String type);
    }
}