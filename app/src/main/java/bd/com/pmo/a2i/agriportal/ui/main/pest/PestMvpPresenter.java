package bd.com.pmo.a2i.agriportal.ui.main.pest;

import bd.com.pmo.a2i.agriportal.ui.base.MvpPresenter;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public interface PestMvpPresenter<V extends PestMvpView>
        extends MvpPresenter<V> {

    void onViewPrepared();
}


