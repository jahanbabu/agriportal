package bd.com.pmo.a2i.agriportal.model;

/**
 * Created by JK on 10/07/17.
 */

public class Block {
    String blockNumber, blockName, union, officerName, officerNumber;

    public Block(String blockNumber, String blockName, String union, String officerName, String officerNumber) {
        this.blockNumber = blockNumber;
        this.blockName = blockName;
        this.union = union;
        this.officerName = officerName;
        this.officerNumber = officerNumber;
    }

    @Override
    public String toString() {
        return "Block{" +
                "blockNumber='" + blockNumber + '\'' +
                ", blockName='" + blockName + '\'' +
                ", union='" + union + '\'' +
                ", officerName='" + officerName + '\'' +
                ", officerNumber='" + officerNumber + '\'' +
                '}';
    }

    public String getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(String blockNumber) {
        this.blockNumber = blockNumber;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getUnion() {
        return union;
    }

    public void setUnion(String union) {
        this.union = union;
    }

    public String getOfficerName() {
        return officerName;
    }

    public void setOfficerName(String officerName) {
        this.officerName = officerName;
    }

    public String getOfficerNumber() {
        return officerNumber;
    }

    public void setOfficerNumber(String officerNumber) {
        this.officerNumber = officerNumber;
    }
}
