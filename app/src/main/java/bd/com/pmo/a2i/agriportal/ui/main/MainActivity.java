package bd.com.pmo.a2i.agriportal.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import javax.inject.Inject;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.ui.base.BaseActivity;
import bd.com.pmo.a2i.agriportal.ui.fragment.BlockFragment;
import bd.com.pmo.a2i.agriportal.ui.main.hat.HatFragment;
import bd.com.pmo.a2i.agriportal.ui.main.notification.NotificationFragment;
import bd.com.pmo.a2i.agriportal.ui.main.pest.PestFragment;
import bd.com.pmo.a2i.agriportal.ui.main.services.block.BlockActivity;
import bd.com.pmo.a2i.agriportal.ui.main.variety.VarietyFragment;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public class MainActivity extends BaseActivity implements MainMvpView, NavigationView.OnNavigationItemSelectedListener {

    @Inject
    MainMvpPresenter<MainMvpView> mPresenter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    FragmentManager fragmentManager;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        return intent;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
                switch (item.getItemId()) {
                    case R.id.navigation_call:
                        goToFragment(HatFragment.newInstance(), "HatFragment");
                        return true;
                    case R.id.navigation_hat:
                        goToFragment(HatFragment.newInstance(), "HatFragment");
                        return true;
                    case R.id.navigation_variety:
                        goToFragment(VarietyFragment.newInstance(), "VarietyFragment");
                        return true;
                    case R.id.navigation_pest:
                        goToFragment(PestFragment.newInstance(), "PestFragment");
                        return true;
                    case R.id.navigation_notifications:
                        goToFragment(NotificationFragment.newInstance(), "NotificationFragment");
                        return true;
                }
                return false;
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);

        setUp();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void setUp() {
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                upIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                            // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_upozila) {
            // Handle the camera action
        } else if (id == R.id.nav_uposohokari) {

        } else if (id == R.id.nav_block) {
            startActivity(new Intent(getApplicationContext(), BlockActivity.class));
        } else if (id == R.id.nav_farmer) {
//            fragmentManager.beginTransaction().add(R.id.fragmentContainer, BlockFragment.newInstance(), "blockFragment").commit();
        } else if (id == R.id.nav_society) {

        } else if (id == R.id.nav_instrument) {

        } else if (id == R.id.nav_dealer) {

        } else if (id == R.id.nav_newspaper) {

        } else if (id == R.id.nav_video) {

        } else if (id == R.id.nav_training) {

        } else if (id == R.id.nav_presentation) {

        } else if (id == R.id.nav_calender) {

        } else if (id == R.id.nav_crop) {

        } else if (id == R.id.nav_album) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void goToFragment(Fragment fragment, String TAG) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainer, fragment, TAG).commit();
    }
}
