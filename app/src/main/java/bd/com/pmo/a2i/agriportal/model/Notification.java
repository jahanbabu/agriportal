package bd.com.pmo.a2i.agriportal.model;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public class Notification {
    String nid, notificationTitle, notificationDetails, notificationType, notificationTime;

    public Notification(String nid, String notificationTitle, String notificationDetails, String notificationType, String notificationTime) {
        this.nid = nid;
        this.notificationTitle = notificationTitle;
        this.notificationDetails = notificationDetails;
        this.notificationType = notificationType;
        this.notificationTime = notificationTime;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getNotificationTitle() {
        return notificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

    public String getNotificationDetails() {
        return notificationDetails;
    }

    public void setNotificationDetails(String notificationDetails) {
        this.notificationDetails = notificationDetails;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getNotificationTime() {
        return notificationTime;
    }

    public void setNotificationTime(String notificationTime) {
        this.notificationTime = notificationTime;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "nid='" + nid + '\'' +
                ", notificationTitle='" + notificationTitle + '\'' +
                ", notificationDetails='" + notificationDetails + '\'' +
                ", notificationType='" + notificationType + '\'' +
                ", notificationTime='" + notificationTime + '\'' +
                '}';
    }
}
