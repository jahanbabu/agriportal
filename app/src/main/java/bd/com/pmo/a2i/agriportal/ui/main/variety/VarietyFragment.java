package bd.com.pmo.a2i.agriportal.ui.main.variety;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;
import javax.inject.Inject;
import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.di.component.ActivityComponent;
import bd.com.pmo.a2i.agriportal.model.Variety;
import bd.com.pmo.a2i.agriportal.ui.base.BaseFragment;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public class VarietyFragment extends BaseFragment implements VarietyMvpView, VarietyAdapter.Callback {

    private static final String TAG = "VarietyFragment";

    @Inject
    VarietyMvpPresenter<VarietyMvpView> mPresenter;

    @Inject
    VarietyAdapter varietyAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @BindView(R.id.hat_recycler_view)
    RecyclerView mRecyclerView;

    public static VarietyFragment newInstance() {
        Bundle args = new Bundle();
        VarietyFragment fragment = new VarietyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hat, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
            varietyAdapter.setCallback(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(varietyAdapter);

        mPresenter.onViewPrepared();
    }

    @Override
    public void onVarietyEmptyViewRetryClick() {

    }

    @Override
    public void updateVariety(List<Variety> itemList) {
        varietyAdapter.addItems(itemList);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

}
