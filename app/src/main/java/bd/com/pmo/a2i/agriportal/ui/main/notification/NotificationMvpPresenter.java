package bd.com.pmo.a2i.agriportal.ui.main.notification;

import bd.com.pmo.a2i.agriportal.ui.base.MvpPresenter;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public interface NotificationMvpPresenter<V extends NotificationMvpView>
        extends MvpPresenter<V> {

    void onViewPrepared();
}


