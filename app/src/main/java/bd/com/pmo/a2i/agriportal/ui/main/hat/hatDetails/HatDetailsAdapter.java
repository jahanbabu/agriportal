package bd.com.pmo.a2i.agriportal.ui.main.hat.hatDetails;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.model.HatItem;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HatDetailsAdapter extends RecyclerView.Adapter<HatDetailsAdapter.ViewHolder> {
    private ArrayList<HatItem> hatItems;
    private Context context;
    private ItemClickListener itemClickListener;

    static class ViewHolder extends RecyclerView.ViewHolder{
        private Context context;
        // each data item is just a string in this case
        @BindView(R.id.itemNameTextView) TextView itemNameTextView;
        @BindView(R.id.itemPriceEditText) EditText itemPriceEditText;
        @BindView(R.id.itemImageView) ImageView itemImageView;
        @BindView(R.id.plusImageView) ImageView plusImageView;
        @BindView(R.id.minusImageView) ImageView minusImageView;

        View view;
        ViewHolder(Context context, View v) {
            super(v);
            this.context = context;
            this.view = v;
            // 2. Bind Butter knife to this view holder
            ButterKnife.bind(this, itemView);
        }
    }

    public HatDetailsAdapter(ArrayList<HatItem> hatItems) {
        this.hatItems = hatItems;
    }

    @Override
    public HatDetailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_hat_item, parent, false);

        return new ViewHolder(context, v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

//        if (hats.get(position).getStartAddress() == null )
//            holder.startAddressTextView.setText("Select Doctor");

        holder.itemNameTextView.setText(hatItems.get(position).getName());
        holder.itemPriceEditText.setText(hatItems.get(position).getPrice());

//        holder.view.setOnClickListener(view -> hatClickListener.blockItemClicked(view, position, "row"));

        holder.plusImageView.setOnClickListener(view -> itemClickListener.itemClicked(view, position, "add"));

        holder.minusImageView.setOnClickListener(view -> itemClickListener.itemClicked(view, position, "minus"));
    }

    public void addItems(List<HatItem> itemList) {
        hatItems.addAll(itemList);
        notifyDataSetChanged();
    }

    public void changePrice(int position, boolean isAdd) {
        if (isAdd){
            hatItems.get(position).setPrice((Integer.parseInt(hatItems.get(position).getPrice()) + 1) + "");
        } else {
            hatItems.get(position).setPrice((Integer.parseInt(hatItems.get(position).getPrice()) - 1) + "");
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return hatItems.size();
    }

    public void setClickListener(ItemClickListener clickListener) {
        this.itemClickListener = clickListener;
    }

    public interface ItemClickListener {
        void itemClicked(View v, int position, String type);
    }
}