package bd.com.pmo.a2i.agriportal.ui.main.hat;

import java.util.List;

import bd.com.pmo.a2i.agriportal.model.Hat;
import bd.com.pmo.a2i.agriportal.ui.base.MvpView;

/**
 * Created by JahangirKabir on 18/10/17.
 */

public interface HatMvpView extends MvpView {

    void updateHat(List<Hat> hatList);
}
