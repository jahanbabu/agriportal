
package bd.com.pmo.a2i.agriportal.model;

import com.google.gson.annotations.SerializedName;

public class GeneralResponse {

    @SerializedName("message")
    private String mMessage;
    @SerializedName("success")
    private Long mSuccess;

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public Long getmSuccess() {
        return mSuccess;
    }

    public void setmSuccess(Long mSuccess) {
        this.mSuccess = mSuccess;
    }
}
