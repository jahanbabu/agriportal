package bd.com.pmo.a2i.agriportal.model;

/**
 * Created by JK on 10/14/17.
 */

public class Instrument {
    String  instrumentName, instrumentType, image;

    public Instrument(String instrumentName, String instrumentType, String image) {
        this.instrumentName = instrumentName;
        this.instrumentType = instrumentType;
        this.image = image;
    }

    @Override
    public String toString() {
        return "Instrument{" +
                "instrumentName='" + instrumentName + '\'' +
                ", instrumentType='" + instrumentType + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public String getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(String instrumentType) {
        this.instrumentType = instrumentType;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
