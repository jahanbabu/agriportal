package bd.com.pmo.a2i.agriportal.di.module;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import bd.com.pmo.a2i.agriportal.BuildConfig;
import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.data.AppDataManager;
import bd.com.pmo.a2i.agriportal.data.DataManager;
import bd.com.pmo.a2i.agriportal.data.db.AppDbHelper;
import bd.com.pmo.a2i.agriportal.data.db.DbHelper;
import bd.com.pmo.a2i.agriportal.data.network.ApiHeader;
import bd.com.pmo.a2i.agriportal.data.network.ApiHelper;
import bd.com.pmo.a2i.agriportal.data.network.AppApiHelper;
import bd.com.pmo.a2i.agriportal.data.prefs.AppPreferencesHelper;
import bd.com.pmo.a2i.agriportal.data.prefs.PreferencesHelper;
import bd.com.pmo.a2i.agriportal.di.ApiInfo;
import bd.com.pmo.a2i.agriportal.di.ApplicationContext;
import bd.com.pmo.a2i.agriportal.di.DatabaseInfo;
import bd.com.pmo.a2i.agriportal.di.PreferenceInfo;
import bd.com.pmo.a2i.agriportal.utils.AppConstants;
import dagger.Module;
import dagger.Provides;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by JahangirKabir on 18/10/17.
 */

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return AppConstants.DB_NAME;
    }

    @Provides
    @ApiInfo
    String provideApiKey() {
        return BuildConfig.API_KEY;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    DbHelper provideDbHelper(AppDbHelper appDbHelper) {
        return appDbHelper;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }

    @Provides
    @Singleton
    ApiHeader.ProtectedApiHeader provideProtectedApiHeader(@ApiInfo String apiKey,
                                                           PreferencesHelper preferencesHelper) {
        return new ApiHeader.ProtectedApiHeader(
                apiKey,
                preferencesHelper.getCurrentUserId(),
                preferencesHelper.getAccessToken());
    }

    @Provides
    @Singleton
    CalligraphyConfig provideCalligraphyDefaultConfig() {
        return new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/source-sans-pro/SourceSansPro-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build();
    }
}
