package bd.com.pmo.a2i.agriportal.ui.main.hat.hatDetails;

import java.util.List;

import bd.com.pmo.a2i.agriportal.model.Block;
import bd.com.pmo.a2i.agriportal.model.HatItem;
import bd.com.pmo.a2i.agriportal.ui.base.MvpView;

/**
 * Created by JahangirKabir on 21/10/17.
 */

public interface HatDetailsMvpView extends MvpView {

    void updateHatItem(List<HatItem> hatItems);
}
