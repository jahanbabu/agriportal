package bd.com.pmo.a2i.agriportal.ui.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tbruyelle.rxpermissions2.RxPermissions;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.sip.WalkieTalkieActivity;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TestMainActivity extends AppCompatActivity {
    RxPermissions rxPermissions;
    boolean allGranted = false;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        ButterKnife.bind(this);
        rxPermissions = new RxPermissions(this);

        if (!allGranted) {
            ShowPermissionDialog();
            return;
        }
    }

    @OnClick(R.id.serviceButton2)
    public void onSIPButtonClick(){
        startActivity(new Intent(TestMainActivity.this, WalkieTalkieActivity.class));
    }

    public void ShowPermissionDialog(){
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.USE_SIP) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(mContext, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

            rxPermissions
                    .request(Manifest.permission.USE_SIP, Manifest.permission.RECORD_AUDIO)
                    .subscribe(granted -> {
                        if (granted) {
                            // All requested permissions are granted
                            allGranted = granted;
                        } else {
                            // At least one permission is denied
                            allGranted = false;
                        }
                    });
        } else {
            allGranted = true;
        }

    }
}
