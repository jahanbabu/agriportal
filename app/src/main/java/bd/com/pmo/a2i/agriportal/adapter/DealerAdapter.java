package bd.com.pmo.a2i.agriportal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import bd.com.pmo.a2i.agriportal.R;
import bd.com.pmo.a2i.agriportal.model.Dealer;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DealerAdapter extends RecyclerView.Adapter<DealerAdapter.ViewHolder> {
    private ArrayList<Dealer> dealers;
    private Context context;
    private DealerClickListener dealerClickListener;

    static class ViewHolder extends RecyclerView.ViewHolder{
        private Context context;
        // each data item is just a string in this case
        @BindView(R.id.dealerNameTextView) TextView dealerNameTextView;
        @BindView(R.id.dealerAddressTextView) TextView dealerAddressTextView;
        @BindView(R.id.dealerNumberTextView) TextView dealerNumberTextView;

        @BindView(R.id.imageview) ImageView itemImageView;
        @BindView(R.id.deleteImageView) ImageView deleteImageView;

        View view;
        ViewHolder(Context context, View v) {
            super(v);
            this.context = context;
            this.view = v;
            // 2. Bind Butter knife to this view holder
            ButterKnife.bind(this, itemView);
        }
    }

    public DealerAdapter(Context context, ArrayList<Dealer> dealers) {
        this.dealers = dealers;
        this.context = context;
    }

    @Override
    public DealerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_dealer, parent, false);

        return new ViewHolder(context, v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.dealerNameTextView.setText(dealers.get(position).getDealerName());
        holder.dealerAddressTextView.setText(dealers.get(position).getDealerAddress());
        holder.dealerNumberTextView.setText(dealers.get(position).getDealerNumber());

        holder.view.setOnClickListener(view -> dealerClickListener.blockItemClicked(view, position, "row"));
        holder.deleteImageView.setOnClickListener(view -> dealerClickListener.blockItemClicked(view, position, "delete"));
    }

    @Override
    public int getItemCount() {
        return dealers.size();
    }

    public void setClickListener(DealerClickListener clickListener) {
        this.dealerClickListener = clickListener;
    }

    public interface DealerClickListener {
        void blockItemClicked(View v, int position, String type);
    }
}