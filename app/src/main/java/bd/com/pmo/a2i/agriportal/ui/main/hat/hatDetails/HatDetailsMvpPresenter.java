package bd.com.pmo.a2i.agriportal.ui.main.hat.hatDetails;

import bd.com.pmo.a2i.agriportal.di.PerActivity;
import bd.com.pmo.a2i.agriportal.ui.base.MvpPresenter;

/**
 * Created by JahangirKabir on 21/10/17.
 */

@PerActivity
public interface HatDetailsMvpPresenter<V extends HatDetailsMvpView> extends MvpPresenter<V> {

    void onViewPrepared();

}
